<?php

//declare(strict_types=1);

return [
    'controllers' => [
        'events' => 'Sunnydevbox\TWEvents\Http\Controllers\API\V1\EventController',
    ],

    'models' => [
        'event' => 'Sunnydevbox\TWEvents\Models\Event',
    ],

    'validators' => [
        'event' => 'Sunnydevbox\TWEvents\Validators\EventValidator',
    ],

    'transformers' => [
        'event' => 'Sunnydevbox\TWEvents\Transformers\EventTransformer',
    ],

    // Bookings Database Tables
    'tables' => [
        'events'    => 'events',
        // 'bookings' => 'bookings',
        // 'booking_rates' => 'booking_rates',
        // 'booking_hierarchy' => 'booking_hierarchy',
        // 'booking_availability' => 'booking_availability',

    ],

    // Bookings Models
    // 'models' => [

    //     'booking' => \Rinvex\Bookings\Models\Booking::class,
    //     'booking_rate' => \Rinvex\Bookings\Models\BookingRate::class,
    //     'booking_availability' => \Rinvex\Bookings\Models\BookingAvailability::class,

    // ],

];
