<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(config('tw-events.tables.events'), function($table) {
            $table->engine = 'InnoDB';

            $table->increments('id')->unsigned();

            $table->morphs('assignable');

            $table->string('label')->nullable();
            $table->string('status')->nullable()->default(false);
            $table->datetime('start_at')->nullable();
            $table->datetime('end_at')->nullable();
            $table->decimal('price')->default('0.00');
            

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(config('tw-events.tables.events'));
    }
}
