<?php
namespace Sunnydevbox\TWEvents\Traits;
use Illuminate\Support\Fluent;
use Illuminate\Contracts\Container\Container;
use Illuminate\Database\Eloquent\Relations\MorphToMany;

trait EventTrait
{ 
    public function events()
    {
        return $this->MorphMany('Sunnydevbox\TWEvents\Models\Event', 'assignable');
    }

}