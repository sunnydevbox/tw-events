<?php

namespace Sunnydevbox\TWEvents;

use Illuminate\Support\ServiceProvider;
use Illuminate\Http\Request;

use Sunnydevbox\TWCore\BaseServiceProvider;


class TWEventsServiceProvider extends BaseServiceProvider
{
    public function boot()
    {
        parent::boot();
    }

    public function registerCommands()
    {
        if ($this->app->runningInConsole()) {

            $this->commands([
                \Sunnydevbox\TWEvents\Console\Commands\PublishConfigCommand::class,
                \Sunnydevbox\TWEvents\Console\Commands\PublishMigrationsCommand::class,
            ]);
        }
    }

    public function mergeConfig()
    {
        return [
           realpath(__DIR__ . '/../config/config.php') => 'tw-events'
        ];
    }

    public function toPublishConfig()
    {
        return [
            __DIR__.'/../config/config.php' => config_path('tw-events.php')
        ];
    }
}