<?php
namespace Sunnydevbox\TWEvents\Models;

use Sunnydevbox\TWCore\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class Event extends BaseModel
{
	use SoftDeletes;
	
    protected $fillable = [
        'label',
        'start_at',
        'end_at',
        'assignable_type',
        'assignable_id',
    ];


    /**
     * Overriding getTable()
     *
     * @return string
     */
    public function getTable()
    {
        return config('tw-events.tables.events');
    }

    /**
     * Get all of the owning commentable models.
     */
    public function assignable()
    {
        return $this->morphTo();
    }
}