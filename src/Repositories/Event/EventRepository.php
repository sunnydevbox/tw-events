<?php
namespace Sunnydevbox\TWEvents\Repositories\Event;

use Sunnydevbox\TWCore\Repositories\TWBaseRepository;

class EventRepository extends TWBaseRepository
{
	protected $fieldSearchable = [
        //'id',
        'label'    => 'like',
    ];
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return config('tw-events.models.event');
    }

    /**
     * Specify Validator class name
     *
     * @return mixed
     */
    public function validator()
    {
        return config('tw-events.validators.event');
    }
}