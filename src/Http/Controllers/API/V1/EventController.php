<?php
namespace Sunnydevbox\TWEvents\Http\Controllers\API\V1;

use Sunnydevbox\TWCore\Http\Controllers\APIBaseController;

class EventController extends APIBaseController
{
    public function __construct(
		\Sunnydevbox\TWEvents\Repositories\Event\EventRepository $repository, 
		\Sunnydevbox\TWEvents\Validators\EventValidator $validator,
		\Sunnydevbox\TWEvents\Transformers\EventTransformer $transformer
	) {
        $this->repository = $repository;
        $this->validator  = $validator;
        $this->transformer = $transformer;
    }
}