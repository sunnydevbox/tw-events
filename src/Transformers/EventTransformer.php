<?php
namespace Sunnydevbox\TWEvents\Transformers;

use Dingo\Api\Http\Request;
//use Dingo\Api\Transformer\Binding;
//use Dingo\Api\Contract\Transformer\Adapter;
use League\Fractal\TransformerAbstract;
use Sunnydevbox\TWEvents\Models\Event;

class EventTransformer extends TransformerAbstract
{
    //public function $defaultIncludes = [];

    public function transform($obj)
    {
        return [
            'id'        	=> (int) $obj->id,
            'label' 	   	=> $obj->label,
            'start_at'		=> $obj->start_at,
            'end_at'		=> $obj->end_at,
        ];
    }
}