<?php
namespace Sunnydevbox\TWEvents\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

class EventValidator extends LaravelValidator {

    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'label'     => 'required|min:3',
            'start_at'  => 'required',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'label'     => 'required|min:3',
            'start_at'  => 'required',
        ]
   ];

}